/* $(document).ready(function() {
    let infosUser = $.get("https://people-app.herokuapp.com/");

    console.log(infosUser);
}); */

/* var infosUser = new Request(get, post, ["https://people-app.herokuapp.com/"]);
console.log(infosUser); */

/* function Get(urlAPI) {
    var httpReq = new XMLHttpRequest();
    httpReq.open("GET", urlAPI, false);
    httpReq.send(null);
    return httpReq.responseText;
}

var jsonObject = JSON.parse(Get(urlAPI));
console.log(jsonObject.title); */

// let apiViaCep = "viacep.com.br/ws/01001000/json/";

var urlAPI = "https://people-app.herokuapp.com/?page=1";

function Get(urlAPI) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.open("GET", urlAPI, false);
    httpRequest.send(null);
    return httpRequest.responseText;
}

let jsonObject = JSON.parse(Get(urlAPI));
console.log(jsonObject["results"][0]["name"]);

let getApiButton = document.querySelector('#getApiButton');

function buttonGetApi() {

    let divStudentInfos = document.querySelector('.t-body');

    let results = jsonObject["results"];

    console.log(results.length);

    for (let cont = 0; cont <= results.length; cont++) {

        divStudentInfos.insertAdjacentHTML("beforeend",
           `<th class="students student-id" scope="row">${results[cont]["id"]}</th>
            <td class="students student-cpf">${results[cont]["cpf"]}</td>
            <td class="students student-name">${results[cont]["name"]}</td>
            <td class="students student-birth-date">${results[cont]["birth_date"]}</td>
            <td class="students student-sex">${results[cont]["sex"]}</td>
            <td class="students student-email">${results[cont]["email"]}</td>
            <td class="students student-phone-number">${results[cont]["mobile"]}</td>`);
    }
}

getApiButton.onclick = buttonGetApi;
