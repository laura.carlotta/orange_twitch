function formValidation() {

    if (userMessage.length > 1 && userMessage.length <= 2000) {
      if (userMessage > 2000) {
        formUserContact.message.focus();
        clickButtonOutputChangeClass(outputMessage, ".output-message-off", "output-message-on");
      }
    }
  }

function clickButtonOutputChangeClass(tagId, outputOff, outputOn) {

  tagId.classList.remove(outputOff);
  tagId.classList.add(outputOn);

  setTimeout(() => {
    tagId.classList.add(outputOff);
  }, 5000);

}

function checkLetterInput(e, focus, tag, classOff, classOn) {

  const charLetter = String.fromCharCode(e.keyCode);

  const patternLetter = '[a-zA-z]';

  const space = ' '

  /* const accentuation = '[àÀáÁãÃâÂäÄèÈéÉêÊëËìÌíÍîÎïÏòÒóÓõÕôÔöÖùÙúÚûÛüÜçÇñÑýÝÿŸÆ¨]' */
  const accentuation = '[`~´^-çÇÆ¨]'

  if (charLetter.match(patternLetter) || charLetter.match(space) || charLetter.match(accentuation)) {
    return true;

  } else {

    focus.focus();
    clickButtonOutputChangeClass(tag, classOff, classOn);
  }
}

function checkLetterAndNumberInput(e, focus, tag, classOff, classOn) {

  const charLetter = String.fromCharCode(e.keyCode);

  const patternLetter = '[a-zA-z]';

  const space = ' '

  const accentuation = '[àÀáÁãÃâÂäÄèÈéÉêÊëËìÌíÍîÎïÏòÒóÓõÕôÔöÖùÙúÚûÛüÜçÇñÑýÝÿŸ]'

  const number = '[0-9]'

  if (charLetter.match(patternLetter) || charLetter.match(space) || charLetter.match(accentuation) || charLetter.match(number) ) {
    return true;

  } else {

    focus.focus();
    clickButtonOutputChangeClass(tag, classOff, classOn);
  }
}

function checkNumberInput(e, focus, tag, classOff, classOn) {

  const charLetter = String.fromCharCode(e.keyCode);

  const patternLetter = '[0-9]';

  if (charLetter.match(patternLetter)) {
    return true;

  } else {

    focus.focus();
    clickButtonOutputChangeClass(tag, classOff, classOn);
  }
}

function checkCepInput(e, focus, tag, classOff, classOn) {

  const charLetter = String.fromCharCode(e.keyCode);

  const patternLetter = '[0-9]';

  const symbols = '[-]'


  if (charLetter.match(patternLetter) || charLetter.match(symbols)) {
    return true;

  } else {

    focus.focus();
    clickButtonOutputChangeClass(tag, classOff, classOn);
  }

}

function checkPhoneInput(e, focus, tag, classOff, classOn) {

  const charLetter = String.fromCharCode(e.keyCode);

  const patternLetter = '[0-9]';

  const symbols = '[()-]'


  if (charLetter.match(patternLetter) || charLetter.match(symbols)) {
    return true;

  } else {

    focus.focus();
    clickButtonOutputChangeClass(tag, classOff, classOn);
  }

}


let userNameId = document.querySelector("#name");
userNameId.addEventListener("keypress", function (e) {

  if (!checkLetterInput(e , userNameId, outputName, "output-name-off", "output-name-on")) {
    e.preventDefault();
  }

});

let userCepId = document.querySelector("#cep");
userCepId.addEventListener("keypress", function (e) {

  if (!checkPhoneInput(e , userCepId, outputCep, "output-cep-off", "output-cep-on")) {
    e.preventDefault();
  }

});

let userStreetId = document.querySelector("#street");
userStreetId.addEventListener("keypress", function(e) {

  if (!checkLetterInput(e, userStreetId, outputStreet, "output-street-off", "output-street-on")) {
    e.preventDefault();
  }

});

let userStreetNumberId = document.querySelector("#streetNumber");
userStreetNumberId.addEventListener("keypress", function (e) {

  if (!checkNumberInput(e , userStreetNumberId, outputStreetNumber, "output-street-number-off", "output-street-number-on")) {
    e.preventDefault();
  }

});

let userComplementId = document.querySelector("#complement");
userComplementId.addEventListener("keypress", function(e) {

  if (!checkLetterAndNumberInput(e, userComplementId, outputComplement, "output-complement-off", "output-complement-on")) {
    e.preventDefault();
  }

});

let userNeighborhoodId = document.querySelector("#neighborhood");
userNeighborhoodId.addEventListener("keypress", function(e) {

  if (!checkLetterInput(e, userNeighborhoodId, outputNeighborhood, "output-neighborhood-off", "output-neighborhood-on")) {
    e.preventDefault();
  }

});

let userCityId = document.querySelector("#city");
userCityId.addEventListener("keypress", function(e) {

  if (!checkLetterInput(e, userCityId, outputCity, "output-city-off", "output-city-on")) {
    e.preventDefault();
  }

});

let userStateId = document.querySelector("#state");
userStateId.addEventListener("keypress", function(e) {

  if (!checkLetterInput(e, userStateId, outputState, "output-state-off", "output-state-on")) {
    e.preventDefault();
  }

});

let userPhoneId = document.querySelector("#phone");
userPhoneId.addEventListener("keypress", function (e) {

  if (!checkPhoneInput(e , userPhoneId, outputPhone, "output-phone-off", "output-phone-on")) {
    e.preventDefault();
  }

});
