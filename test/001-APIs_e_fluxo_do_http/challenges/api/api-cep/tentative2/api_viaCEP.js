 $(document).ready(function() {

    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#street").val("");
        $("#neighborhood").val("");
        $("#city").val("");
        $("#state").val("");
    }


    
    //Quando o campo cep perde o foco.
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var userCEP = $(this).val().replace(/\D/g, '');
        let inputCep = document.querySelector('#cep');

        const divDontCepFind = document.querySelector( '#outputCep' );
        
        //Verifica se campo cep possui valor informado.
        if (userCEP != "") {

            //Expressão regular para validar o CEP.
            var cepValidate = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(cepValidate.test(userCEP)) {
                
                divDontCepFind.textContent = '';

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#street").val("...");
                $("#neighborhood").val("...");
                $("#city").val("...");
                $("#state").val("...");
                

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ userCEP +"/json/?callback=?", function(data) {

                    if (!("erro" in data)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#street").val(data.logradouro);
                        $("#neighborhood").val(data.bairro);
                        $("#city").val(data.localidade);
                        $("#state").val(data.uf);
                        
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        divDontCepFind.textContent = 'Cep não encontrado';
                        clickButtonOutputChangeClass(divDontCepFind, 'output-cep-off', 'output-cep-on')
                        inputCep.value = '';
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                divDontCepFind.textContent = 'Cep não encontrado';
                clickButtonOutputChangeClass(divDontCepFind, 'output-cep-off', 'output-cep-on')
                inputCep.value = '';
                
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });
});
