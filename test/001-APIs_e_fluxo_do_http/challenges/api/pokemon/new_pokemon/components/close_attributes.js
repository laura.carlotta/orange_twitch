const CloseAllAttributesButton = () => {
    const closeAllAttributesButton = document.createElement( 'button' );
    closeAllAttributesButton.classList.add( 'button' , 'close-button' );
    closeAllAttributesButton.insertAdjacentText( 'beforeend' , 'X');

    closeAllAttributesButton.addEventListener( 'click' , closeAttributes ); 
    return closeAllAttributesButton;
}

const closeAttributes = ( event ) => {
    const closeAllAttributesButton = event.target;
    const closeMoreInfosPokemon = closeAllAttributesButton.parentElement;
    
    closeMoreInfosPokemon.addEventListener( 'click' , function () {
        let divAttributes = document.querySelector( '.get-tag-attributes-pokemon-day' );
        divAttributes.classList.remove( 'show-attributes' );
        divAttributes.classList.add( 'hide-attributes' );
    });

    return closeAllAttributesButton;
}

export default CloseAllAttributesButton;