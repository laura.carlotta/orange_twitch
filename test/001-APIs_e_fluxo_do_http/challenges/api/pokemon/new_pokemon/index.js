import ShowMoreAttributesButton from './components/show_attributes.js';
import CloseAllAttributesButton from './components/close_attributes.js';

(() => {

    let randomNumber = Math.round( Math.random() * 898 + 1 );
    
    let pokemonNameJSON = [];
    let pokemonObjectUrlImageJSON = [];
    let pokemonAttackJSON = [];
    let pokemonDefensJSON = [];
    let pokemonSpeciaJSON = [];
    let pokemonSpeedJSON = [];
    let pokemonTypesJSON = [];
    let responseApiPokemon = [];
    let pokemonJSON = [];

    let searchPokemon;

    let cont = [ 0 , 1 ];

    const urlApi = new XMLHttpRequest();
    const searchPokemonByNameOrId = new XMLHttpRequest();    

    let searchAPI = ( value , variabel ) => {
        variabel.open( "GET" , "https://pokeapi.co/api/v2/pokemon/" + value );
    }

    searchAPI( randomNumber , urlApi);
    urlApi.addEventListener( "load", () => {
        loadPage(urlApi, cont[0]);

        showPokemonDay(
            pokemonObjectUrlImageJSON[0],
            pokemonNameJSON[0],
            'data-pokemon' 
        );

        moreAttributes(
            pokemonTypesJSON[0],
            pokemonSpeedJSON[0],
            pokemonAttackJSON[0],
            pokemonDefensJSON[0],
            pokemonSpeciaJSON[0],
            'data-pokemon',
            'get-tag-attributes-pokemon-day'
        );

        const buttonReload = document.querySelector( '[data-reload-pokemon]' );
        buttonReload.addEventListener( 'click' , reloadPokemon );

    });

    const loadPage = ( variable , cont ) => {
        responseApiPokemon = variable.responseText;
        pokemonJSON.push(JSON.parse( responseApiPokemon ));
        pokemonNameJSON.push(pokemonJSON[cont].name);
        pokemonObjectUrlImageJSON.push(pokemonJSON[cont].sprites.other.dream_world.front_default);
        
        if( pokemonObjectUrlImageJSON[cont] == null ) {
            
            pokemonObjectUrlImageJSON.splice(0,1)
            pokemonObjectUrlImageJSON.push(pokemonJSON[cont].sprites.front_default);
            
        }

        pokemonAttackJSON.push(pokemonJSON[cont].stats[1].base_stat);
        pokemonDefensJSON.push(pokemonJSON[cont].stats[2].base_stat);
        pokemonSpeciaJSON.push(pokemonJSON[cont].stats[3].base_stat);
        pokemonSpeedJSON.push(pokemonJSON[cont].stats[5].base_stat);
        typesPokemon(pokemonJSON[cont]);
    
        return pokemonJSON;

    }

    function reloadPokemon() {
        location.reload();  
    }

    const typesPokemon = ( list ) => {
        const listTypes = list.types;
        listTypes.map( items => {
            pokemonTypesJSON.push( " " + items.type.name + " " );
            return items;
        });
    }

    const showPokemonDay = ( image , name , dataTagVariabel) => {
        const divShowPokemon = document.querySelector( `[${dataTagVariabel}]` );
        const sectionPokemon = document.createElement( 'section' );
        sectionPokemon.classList.add( 'get-pokemon-remove' );
        const htmlImage = `<img class="pokemon-day-image" src="${ image }">`;
        const htmlName = `<div class="pokemon-day-name">${ name }</div>`;
        sectionPokemon.insertAdjacentHTML( 'beforeend' , htmlImage );
        sectionPokemon.insertAdjacentHTML( 'beforeend' , htmlName );
        sectionPokemon.insertAdjacentElement( 'beforeend' , ShowMoreAttributesButton() );
        divShowPokemon.appendChild( sectionPokemon );


        return divShowPokemon;
    }

    const moreAttributes = ( types, speed , attack , defense , specialAttack , dataTagVariabel , tagShowAttributesTarget ) => {

        const divShowPokemon = document.querySelector( `[${dataTagVariabel}]` );

        const divMoreAttributes = document.createElement( 'section' );
        divMoreAttributes.classList.add( 'hide-attributes' );
        divMoreAttributes.classList.add( tagShowAttributesTarget );

        const htmlTypes = /* html */
            `<div class="pokemon-info-attibutes">
                <span class="title-attributes">Tipo :</span>
                <span class="value-attributes">${ types }</span>
            </div>`;

        const htmlSpeed = /* html */
            `<div class="pokemon-info-attibutes">
                <span class="title-attributes">Velocidade :</span>
                <span class="value-attributes">${ speed }</span>
            </div>`;

        const htmlAttack = /* html */
            `<div class="pokemon-info-attibutes">
                <span class="title-attributes">Ataque :</span>
                <span class="value-attributes">${ attack }</span>
            </div>`;

        const htmlDefense = /* html */
            `<div class="pokemon-info-attibutes">
                <span class="title-attributes">Defesa :</span>
                <span class="value-attributes"> ${ defense }</span>
            </div>`;

        const htmlSpecialAttack = /* html */
            `<div class="pokemon-info-attibutes">
                <span class="title-attributes">Ataque-especial :</span>
                <span class="value-attributes">${ specialAttack }</span>
            </div>`;

        divMoreAttributes.insertAdjacentHTML( 'beforeend', htmlTypes );
        divMoreAttributes.insertAdjacentHTML( 'beforeend', htmlSpeed );
        divMoreAttributes.insertAdjacentHTML( 'beforeend', htmlAttack );
        divMoreAttributes.insertAdjacentHTML( 'beforeend', htmlDefense );
        divMoreAttributes.insertAdjacentHTML( 'beforeend', htmlSpecialAttack );
        divMoreAttributes.insertAdjacentElement( 'beforeend', CloseAllAttributesButton () );
        divShowPokemon.appendChild(divMoreAttributes);

        return divShowPokemon;
    }

    
    const searchButton = document.querySelector( '[data-search-button]' );
    searchButton.addEventListener( 'click', function() {
        const inputPokemonSearch = document.getElementById( 'input-search-pokemon').value;
        console.log(inputPokemonSearch);

        searchPokemon = inputPokemonSearch;

        searchAPI(searchPokemon , searchPokemonByNameOrId);
        console.log(pokemonJSON);
        searchPokemonByNameOrId.addEventListener( 'load' , () => {
            loadPage(searchPokemonByNameOrId, cont[1]);
            console.log(pokemonJSON);
            showPokemonDay(
                pokemonObjectUrlImageJSON[1],
                pokemonNameJSON[1],
                'data-what-your-pokemon'
            );

            moreAttributes(
                pokemonTypesJSON[1],
                pokemonSpeedJSON[1],
                pokemonAttackJSON[1],
                pokemonDefensJSON[1],
                pokemonSpeciaJSON[1],
                'data-what-your-pokemon', 
                'get-tag-attributes-what-your-pokemon'
            );
        });
        
        searchPokemonByNameOrId.send()
        
    } );

    urlApi.send();

}) ();
                                                                                                      