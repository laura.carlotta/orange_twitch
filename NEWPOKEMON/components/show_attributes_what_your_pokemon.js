const ShowMoreAttributesButtonWhatsYourPokemon = () => {
    const showMoreAttributesButton = document.createElement( 'button' );
    showMoreAttributesButton.classList.add( 'button' , 'more-attributes' );
    showMoreAttributesButton.insertAdjacentText( 'beforeend' , "+Atributos" );

    showMoreAttributesButton.addEventListener( 'click', showAttributesWhatYourPokemon );

    return showMoreAttributesButton;
}

const showAttributesWhatYourPokemon = ( event ) => {
    const showMoreAttributesButton = event.target; // poderia ser qualquer nome.
    const moreAttributes = showMoreAttributesButton.parentElement;
    
    moreAttributes.addEventListener( 'click' , function () {
        
        let divAttributes = document.querySelector( '.get-tag-attributes-what-your-pokemon' );
        divAttributes.classList.remove( 'hide-attributes' );
        divAttributes.classList.add( 'show-attributes' );
        
        
    });
}
export default ShowMoreAttributesButtonWhatsYourPokemon;
