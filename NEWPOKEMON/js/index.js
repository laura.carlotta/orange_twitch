import ShowMoreAttributesButtonPokemonDay from '../components/show_attributes_pokemon_day.js';
import CloseAllAttributesButtonPokemonDay from '../components/close_attributes_pokemon_day.js';
import ShowMoreAttributesButtonWhatsYourPokemon from '../components/show_attributes_what_your_pokemon.js'
import CloseAllAttributesButtonWhatsYourPokemon from '../components/close_attributes_what_your_pokemon.js'

(() => {

    let randomNumber = Math.round( Math.random() * 898 + 1 );
    
    let pokemonNameJSON = [];
    let pokemonObjectUrlImageJSON = [];
    let pokemonAttackJSON = [];
    let pokemonDefensJSON = [];
    let pokemonSpeciaJSON = [];
    let pokemonSpeedJSON = [];
    let pokemonTypesJSON = [];
    let responseApiPokemon = [];
    let pokemonJSON = [];

    let searchPokemon;

    let cont = [ 0 , 1 ];

    const urlApi = new XMLHttpRequest();   

    let searchAPI = ( value , variabel ) => {
        variabel.open( "GET" , "https://pokeapi.co/api/v2/pokemon/" + value );
    }

    searchAPI( randomNumber , urlApi );
    urlApi.addEventListener( "load", () => {
        loadPage(urlApi, cont[0]);

        showPokemon(
            pokemonObjectUrlImageJSON[0],
            pokemonNameJSON[0],
            'data-pokemon',
            ShowMoreAttributesButtonPokemonDay()
        );

        moreAttributes(
            pokemonTypesJSON,
            pokemonSpeedJSON[0],
            pokemonAttackJSON[0],
            pokemonDefensJSON[0],
            pokemonSpeciaJSON[0],
            'data-pokemon',
            'get-tag-attributes-pokemon-day',
            CloseAllAttributesButtonPokemonDay()
        );

        const buttonReload = document.querySelector( '[data-reload-pokemon]' );
        buttonReload.addEventListener( 'click' , reloadPokemon );

    });

    const loadPage = ( variable , cont ) => {
        responseApiPokemon = variable.responseText;

        if (pokemonJSON.length > 1) {
            pokemonNameJSON.pop();
            pokemonObjectUrlImageJSON.pop();
            pokemonAttackJSON.pop();
            pokemonDefensJSON.pop();
            pokemonSpeciaJSON.pop();
            pokemonSpeedJSON.pop();
            pokemonTypesJSON.pop();
            pokemonJSON.pop();
        }

        pokemonJSON.push( JSON.parse( responseApiPokemon ) );
        pokemonNameJSON.push( pokemonJSON[ cont ].name );
        pokemonObjectUrlImageJSON.push( pokemonJSON[ cont ].sprites.other.dream_world.front_default );
        
        if( pokemonObjectUrlImageJSON[ cont ] == null ) {
            
            pokemonObjectUrlImageJSON.splice( 0 , 1 )
            pokemonObjectUrlImageJSON.push( pokemonJSON[ cont ].sprites.front_default );
        
        }

        pokemonAttackJSON.push( pokemonJSON[ cont ].stats[ 1 ].base_stat );
        pokemonDefensJSON.push( pokemonJSON[ cont ].stats[ 2 ].base_stat );
        pokemonSpeciaJSON.push( pokemonJSON[ cont ].stats[ 3 ].base_stat );
        pokemonSpeedJSON.push( pokemonJSON[ cont ].stats[ 5 ].base_stat );
        typesPokemon( pokemonJSON[ cont ] );
    
        return pokemonJSON;

    }

    function reloadPokemon() {
        location.reload();  
    }

    const typesPokemon = ( list ) => {

        const searchButtonAboutTypePokemon = document.querySelector('[data-search-button]')
        searchButtonAboutTypePokemon.addEventListener( 'click' , function () {
            if (pokemonTypesJSON.length >= 1) {
                pokemonTypesJSON.splice(0, pokemonTypesJSON.length)
            }
        })

        const listTypes = list.types;
        listTypes.map( items => {
            pokemonTypesJSON.push( " " + items.type.name + " " );
            return pokemonTypesJSON
        });
        
    };

    const showPokemon = ( image , name , dataTagVariabel, button ) => {
        let divShowPokemon = document.querySelector( `[${ dataTagVariabel }]` );
        divShowPokemon.textContent = '';
        let sectionPokemon = document.createElement( 'section' );
        sectionPokemon.classList.add( 'get-pokemon-remove' );
        let htmlImage = `<img class="pokemon-day-image" src="${ image }">`;
        let htmlName = `<div class="pokemon-day-name">${ name }</div>`;
        sectionPokemon.insertAdjacentHTML( 'beforeend' , htmlImage );
        sectionPokemon.insertAdjacentHTML( 'beforeend' , htmlName );
        sectionPokemon.insertAdjacentElement( 'beforeend' , button );
        divShowPokemon.appendChild( sectionPokemon );

        // return divShowPokemon;
    }

    const moreAttributes = ( 
        types, speed , attack , defense , specialAttack , dataTagVariabel , tagShowAttributesTarget, button ) => {

        const divShowPokemon = document.querySelector( `[${ dataTagVariabel }]` );

        const divMoreAttributes = document.createElement( 'section' );
        divMoreAttributes.classList.add( 'hide-attributes' );
        divMoreAttributes.classList.add( tagShowAttributesTarget );

        const htmlTypes = /* html */
            `<div class="pokemon-info-attibutes">
                <span class="title-attributes">Tipo :</span>
                <span class="value-attributes">${ types }</span>
            </div>`;

        const htmlSpeed = /* html */
            `<div class="pokemon-info-attibutes">
                <span class="title-attributes">Velocidade :</span>
                <span class="value-attributes">${ speed }</span>
            </div>`;

        const htmlAttack = /* html */
            `<div class="pokemon-info-attibutes">
                <span class="title-attributes">Ataque :</span>
                <span class="value-attributes">${ attack }</span>
            </div>`;

        const htmlDefense = /* html */
            `<div class="pokemon-info-attibutes">
                <span class="title-attributes">Defesa :</span>
                <span class="value-attributes"> ${ defense }</span>
            </div>`;

        const htmlSpecialAttack = /* html */
            `<div class="pokemon-info-attibutes">
                <span class="title-attributes">Ataque-especial :</span>
                <span class="value-attributes">${ specialAttack }</span>
            </div>`;

        divMoreAttributes.insertAdjacentHTML( 'beforeend', htmlTypes );
        divMoreAttributes.insertAdjacentHTML( 'beforeend', htmlSpeed );
        divMoreAttributes.insertAdjacentHTML( 'beforeend', htmlAttack );
        divMoreAttributes.insertAdjacentHTML( 'beforeend', htmlDefense );
        divMoreAttributes.insertAdjacentHTML( 'beforeend', htmlSpecialAttack );
        divMoreAttributes.insertAdjacentElement( 'beforeend', button );
        divShowPokemon.appendChild( divMoreAttributes );

        return divShowPokemon;
    }

      
    const searchButton = document.querySelector( '[ data-search-button ]' );
    searchButton.addEventListener( 'click', function() {

        const searchPokemonByNameOrId = new XMLHttpRequest();

        let inputPokemonSearchWithValue = document.getElementById( 'input-search-pokemon').value;
        let inputPokemonSearchTag = document.getElementById( 'input-search-pokemon');
        let divWhatYourPokemon = document.querySelector( '.what-your-pokemon' );
        searchPokemon = inputPokemonSearchWithValue;

        searchAPI( searchPokemon , searchPokemonByNameOrId );
        console.log( pokemonJSON );
        searchPokemonByNameOrId.addEventListener( 'load' , () => {

            const pokemonErrorMessageInput = document.querySelector( '[data-test-error]' );
            let htmlErrorMessage;

            if ( inputPokemonSearchTag.length == '' || inputPokemonSearchWithValue <=0 || !inputPokemonSearchWithValue.indexOf(' ') == -1) {

                pokemonErrorMessageInput.textContent = "";
                divWhatYourPokemon.textContent = "";
                htmlErrorMessage = `Digite um Id ou nome válido!`;
                pokemonErrorMessageInput.insertAdjacentText( 'beforeend' , htmlErrorMessage );
                console.log(pokemonObjectUrlImageJSON);

            } else if ( searchPokemonByNameOrId.status != 200 ) {

                pokemonErrorMessageInput.textContent = "";
                divWhatYourPokemon.textContent = "";
                htmlErrorMessage = 'Desculpe, não foi possível encontrar nenhum pokémon.'
                pokemonErrorMessageInput.insertAdjacentText( 'beforeend', htmlErrorMessage );
            
            } else {
                
                pokemonErrorMessageInput.textContent = "";
                
                loadPage( searchPokemonByNameOrId , cont[ 1 ] );
                console.log( pokemonJSON );

                showPokemon(
                    pokemonObjectUrlImageJSON[ 1 ],
                    pokemonNameJSON[ 1 ],
                    'data-what-your-pokemon',
                    ShowMoreAttributesButtonWhatsYourPokemon()
                );

                moreAttributes(
                    pokemonTypesJSON,
                    pokemonSpeedJSON[ 1 ],
                    pokemonAttackJSON[ 1 ],
                    pokemonDefensJSON[ 1 ],
                    pokemonSpeciaJSON[ 1 ],
                    'data-what-your-pokemon', 
                    'get-tag-attributes-what-your-pokemon', 
                    CloseAllAttributesButtonWhatsYourPokemon()
                );

            }

            inputPokemonSearchTag.value = '';
            inputPokemonSearchTag.focus();

        });
        
        searchPokemonByNameOrId.send();
        
    } );

    document.addEventListener( 'keypress' , function ( e ) {

        if ( e.key === 'Enter' ) {
        
            const btn = document.querySelector( '[ data-search-button ]' );
            let inputPokemonSearchTag = document.getElementById( 'input-search-pokemon');

            if ( !inputPokemonSearchTag.value == '' ) {

                btn.click();

            }

        }
        

    });

    urlApi.send();

}) ();
                                                                                                      